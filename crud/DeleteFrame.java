package crud;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class DeleteFrame {

	DeleteFrame() {
		JFrame frame=new JFrame("Delete");
		 frame.setSize(400,500);
		 
		 JLabel ID=new JLabel("ID");
		 ID.setBounds(23,45,100,30);
		 frame.add(ID);
		  
		 JTextField IDField=new JTextField();
		 IDField.setBounds(150,45,100,30);
		 frame.add(IDField);
		 
		 JButton DeleteButton=new JButton();
		 DeleteButton.setText("Delete");
		 DeleteButton.setBounds(150,100,100,40);
		 frame.add(DeleteButton);
		 
		 DeleteButton.addActionListener(new ActionListener(){  	 
			 public void actionPerformed(ActionEvent e){  
				 try{  
					 Class.forName("oracle.jdbc.driver.OracleDriver");  
					 Connection con=DriverManager.getConnection
					 ("jdbc:oracle:thin:@localhost:1521:xe","system","admin");  
					 PreparedStatement stmt=con.prepareStatement("delete from studentdetails1 where id=(?)");  
					 stmt.setString(1,IDField.getText());
				  
					 
					 int i=stmt.executeUpdate();  
					 JOptionPane.showMessageDialog(frame,i+" record deleted"); 
					 System.out.println(i+" record deleted");    
					 con.close();  
					   
					 }catch(Exception e1){ 
						 System.out.println(e1);}  
					   
			 		}
		             
		        }  );
		 
		 
		 JButton goBack=new JButton("Go Back");
			goBack.setBounds(150,250,100,30);
			goBack.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
				 Mainframe goback1=new Mainframe();
				 frame.dispose();
			         
			    }  
			});  
			frame.add(goBack); 	 
 	 frame.setLayout(null);
		 frame.setVisible(true); 
	}

}
