package crud;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

 
public class Mainframe {
	public Mainframe(){
		JFrame frame=new JFrame("CRUD");
		 frame.setSize(400,500);
		 JButton AddButton=new JButton();
		 AddButton.setText("Add");
		 AddButton.setBounds(100,100,100,40);
		 frame.add(AddButton); 
		 AddButton.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
				 AddFrame Add=new AddFrame();
				 frame.dispose();
		             
		        }  
		    });  
		 
		 JButton UpdateButton=new JButton();
		 UpdateButton.setText(" Update");
		 UpdateButton.setBounds(100,200,100,40);
		 UpdateButton.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
				 UpdateFrame UpdateButton1=new UpdateFrame();
		         frame.dispose();    
		        }  
		    });  
		 frame.add(UpdateButton);
		 
		 
		 
		 JButton DeleteButton=new JButton();
		 DeleteButton.setText("Delete");
		 DeleteButton.setBounds(100,300,100,40);
		 frame.add(DeleteButton);
		 
		 DeleteButton.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
				 DeleteFrame Delete=new DeleteFrame();
		         frame.dispose();    
		        }  
		    });  
		 
		 
		  
		 
//		 JButton SignUp=new JButton();
//		 SignUp.addActionListener(new ActionListener(){  
//			 public void actionPerformed(ActionEvent e){  
//				 SignUp signupbutton=new SignUp();
//		             
//		        }  
//		    });  
		 
	 
		 frame.setLayout(null);
		 frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		Mainframe mainframe=new Mainframe();
	}

}
