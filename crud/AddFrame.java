package crud;
import java.sql.*;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import signup.SignUp;
import javax.swing.*;
public class AddFrame {

	public AddFrame() {

		JFrame frame=new JFrame("Add Details");
		
		 frame.setSize(400,500);
		 
		 JLabel ID=new JLabel("ID");
		 ID.setBounds(23,45,100,30);
		 frame.add(ID);
		  
		 JTextField IDField=new JTextField();
		 IDField.setBounds(150,45,100,30);
		 frame.add(IDField);
		 
		 JLabel Student_name=new JLabel("Student name");
		 Student_name.setBounds(23,100,150,30);
		 frame.add(Student_name);
		 
		 JTextField Student_nameField=new JTextField();
		 Student_nameField.setBounds(150,100,100,30);
		 frame.add(Student_nameField);
		  
		 JLabel city_name=new JLabel("Enter the city name");
		 city_name.setBounds(23,150,100,30);
		 frame.add(city_name);
		 
		 JTextField city_nameField=new JTextField();
		 city_nameField.setBounds(150,150,100,30);
		 frame.add(city_nameField);
		  
		 JButton Add=new JButton();
		 Add.setText("Add");
		 Add.setBounds(150,200,100,30);
		 Add.addActionListener(new ActionListener(){  
			 
			 public void actionPerformed(ActionEvent e){  
				 try{  
					 Class.forName("oracle.jdbc.driver.OracleDriver");  
					   
					 Connection con=DriverManager.getConnection
					 ("jdbc:oracle:thin:@localhost:1521:xe","system","admin");  
					   
					 PreparedStatement stmt=con.prepareStatement("insert into studentdetails1 values(?,?,?)");  
					 
					 stmt.setString(1,IDField.getText());
					 stmt.setString(2,Student_nameField.getText());  
					 stmt.setString(3,city_nameField.getText()); 
					 
					 int i=stmt.executeUpdate();  
					 JOptionPane.showMessageDialog(frame,i+"record inserted"); 
					 System.out.println(i+" record inserted");    
					 con.close();  
					   
					 }catch(Exception e1){ 
						 System.out.println(e1);}  
					   
			 		}
		             
		        }  );
	JButton goBack=new JButton("Go Back");
	goBack.setBounds(150,250,100,30);
	goBack.addActionListener(new ActionListener(){  
		 public void actionPerformed(ActionEvent e){  
			 Mainframe goback1=new Mainframe();
			 frame.dispose();
	             
	        }  
	    });  
	frame.add(goBack);
 	 frame.add(Add);
	 frame.setLayout(null);
	 frame.setVisible(true);
	}

}
