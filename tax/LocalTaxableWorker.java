package tax;

public class LocalTaxableWorker extends StateTaxableWorker {
	String city_name;
	int city_tax;

	public LocalTaxableWorker(String name, int hourly_pay_rate, int federal_tax_rate, String Statename,
			int StatetaxRate,String city_name,int city_tax) {
		super(name, hourly_pay_rate, federal_tax_rate, Statename, StatetaxRate);
		this.city_name=city_name;
		this.city_tax=city_tax;
	} 
	public String info() {
		return "name: "+this.name+"\nhourly pay rate"+this.hourly_pay_rate+"\nfedaral tax rate:"+this.federal_tax_rate+"\nStatename:"+this.Statename+"\nStatetaxRate:"+this.StatetaxRate+"\ncityname:"+this.city_name+"\ncity tax "+this.city_tax ;
	}
	
	public double taxWithheld(int grossPay) {
		double tax_with_held=grossPay*federal_tax_rate*StatetaxRate*city_tax;
		return tax_with_held;		
	}	
	
	

}
