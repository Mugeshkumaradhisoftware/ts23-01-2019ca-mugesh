package tax;

public class TaxableWorker {
	String name;
	int hourly_pay_rate;
	int federal_tax_rate;
	public TaxableWorker(String name,int hourly_pay_rate,int federal_tax_rate) {
		this.name=name;
		this.hourly_pay_rate=hourly_pay_rate;
		this.federal_tax_rate=federal_tax_rate;
	}
	public String workerInfo() {
		return "name: "+this.name+"hourly pay rate"+this.hourly_pay_rate+"fedaral tax rate"+this.federal_tax_rate;
	}
	public double grossPay(int payrate) {
		double grossPay=hourly_pay_rate*payrate;
		return grossPay;
	}
	public double taxWithheld(int grossPay) {
		double tax_with_held=grossPay*federal_tax_rate;
		return tax_with_held;		
	}

}
