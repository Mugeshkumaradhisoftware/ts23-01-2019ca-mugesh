package tax;

public class StateTaxableWorker extends TaxableWorker {
		String Statename;
		int StatetaxRate;
	public StateTaxableWorker(String name, int hourly_pay_rate, int federal_tax_rate,String Statename,int StatetaxRate ) {
		super(name, hourly_pay_rate, federal_tax_rate);
		this.Statename=Statename;
		this.StatetaxRate=StatetaxRate;
		}
	public String info() {
		return "name: "+this.name+"hourly pay rate"+this.hourly_pay_rate+"fedaral tax rate:"+this.federal_tax_rate+"Statename:"+this.Statename+"StatetaxRate:"+this.StatetaxRate;
	}
	
	public double taxWithheld(int grossPay) {
		double tax_with_held=grossPay*federal_tax_rate*StatetaxRate;
		return tax_with_held;		
	}	
	
} 
